    var callgetlinks = false;
    var requiredAmount = 0;
    var counter = 0;
    var a1, fullname1, headline1, location1, currentUrl;
    var newhistory = history.pushState;
    history.pushState = function() {
        if (callgetlinks == true) {

            getLinks();
        }
        newhistory.apply(history, arguments);
    }


    var fetchedData = [];

    function getLinks() {

        setTimeout(function() {

            window.scrollTo(0, 1000);
            setTimeout(function() {


                $(".search-results ul li").each(function() {
                    var data = {};
                    console.log(counter);
                    console.log("fetching....");
                    var a = $('.search-result__result-link', $(this)).attr("href");
                    if (typeof a != 'undefined' && a != null && a != "#") {
                        a1 = a;
                        data.url = a;
                    }

                    window.scrollTo(0, 900);

                    var fullname = $(".name.actor-name", $(this)).text();
                    fullname = fullname.replace(/\s{2,10}/g, ' ').trim();
                    if (typeof fullname != 'undefined' && fullname != null && fullname != "#") {
                        fullname1 = fullname;
                        data.name = fullname;
                    }
                    window.scrollTo(0, 900);

                    var headline = $('.subline-level-1', $(this)).text();
                    headline = headline.replace(/\s{2,10}/g, ' ').trim();
                    if (typeof headline != 'undefined' && headline != null && headline != "#") {
                        headline1 = headline;
                        data.head = headline;
                    }
                    window.scrollTo(0, 900);

                    var location = $('.subline-level-2', $(this)).text();
                    location = location.replace(/\s{2,10}/g, ' ').trim();
                    if (typeof location != 'undefined' && location != null && location != "#") {
                        location1 = location;
                        data.loc = location;
                    }
                    /* if (!data.url || !data.name || !data.loc || !data.name.length) return;

                    fetchedData.push(data); */
                    if (data.url != null && data.name != null && data.loc != null && data.name.length) {
                        counter += 1;

                        fetchedData.push(data);
                    }

                });
                //security check
                var check1 = "LinkedIn: Log In or Sign Up";
                var check2 = $('title').text()

                if ($("h2").hasClass("Sans-17px-black-85% b0 p0")) {
                    chrome.extension.sendMessage({ cmd: "close" });
                    return;
                }

                if (check1 == check2 || !check2) {
                    chrome.extension.sendMessage({ cmd: "close" });
                    return;
                }

                chrome.extension.sendMessage({ cmd: "saveLinks", data: fetchedData, counter: counter });

                if (fetchedData.length < requiredAmount) {
                    callgetlinks = true;
                    setTimeout(function() {
                        $("button.next").trigger("click");
                    }, Math.floor(Math.random() * (15000 - 3000) + 3000))

                } else if (fetchedData.length > requiredAmount) {
                    console.log("stack over flow");
                    //$("button.next").trigger("click");
                } else if (fetchedData.length == requiredAmount) {
                    alert("Data collecting completed");
                }
            }, Math.floor(Math.random() * (5000 - 3000) + 3000))
        }, Math.floor(Math.random() * (5000 - 3000) + 3000));

    }

    function getComand(data) {

        switch (data.cmd) {

            case "getLinks":
                requiredAmount = data.amount;
                fetchedData = data.data;
                getLinks();
                break;


        }

    }

    chrome.extension.sendMessage({ cmd: "amReady" }, getComand);

    chrome.runtime.onMessage.addListener(
        function(response, sender, sendResponse) {
            if (response.cmd == "stop") {
                callgetlinks == false;
            }
            if (response.cmd == "start") {
                callgetlinks == true;
                $("button.next").trigger("click");

            }
        })