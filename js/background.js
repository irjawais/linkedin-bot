var projectAmount1 = 0;
var current = 0;
var status = "getLinks";
var code;
var getresult1 = [];
var url;
var position = false;
var tab1;
var tab2;
var fetchedDataB = [];
var buttonStatus;
var oldStatus = "";
buttonStatus = "START";
var signal = "START";
var processStatus = "START";
var processSignal = "START";
chrome.windows.onRemoved.addListener(function(windowId) {
    buttonStatus = "START";
    processStatus = "START";
});
var func = function(projectName, projectUrl, projectAmount) {
    var config = {
        url: projectUrl,
        width: 1200,
        height: 700,
        top: 10,
        left: 10,
        type: "popup"
    }

    chrome.windows.create(config, function(wnd) {
        code = localStorage.getItem('LicenseCode');
        status = "getLinks";
        chrome.tabs.onUpdated.addListener(
            function(tabId, changeInfo, tab) {
                tab1 = wnd.id;
                tab2 = wnd.tabs[0].id;
                if (changeInfo.status === "complete" && wnd.tabs[0].id == tabId) {
                    chrome.tabs.executeScript(tabId, { file: "script.js", runAt: "document_idle" },
                        function(result) {
                            // Process |result| here (or maybe do nothing at all).
                        }
                    );
                }
            }
        );

    });


    projectAmount1 = projectAmount;
};

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.msg == "startFunc") func(request.projectName, request.projectUrl, request.projectAmount);

        if (request.msg == "start") {
            status = oldStatus;
            //position = oldPosition;
            chrome.tabs.sendMessage(tab2, { cmd: "start" }, function(response) {});

        }
        if (request.msg == "stop") {
            oldStatus = status;
            // oldPosition = position;;
            //position = "pause"
            status = "pause";
            //chrome.tabs.sendMessage(tab2, { cmd: "start" }, function(response) {});
        }
    }
);


chrome.runtime.onMessage.addListener(
    function(response, sender, sendResponse) {
        if (!response.cmd) return;
        if (response.cmd == "saveLinks") {
            data = response.data;
            fetchedDataB = data;
            console.log(fetchedDataB);
            if (fetchedDataB.length > projectAmount1) {
                fetchedDataB = fetchedDataB.slice(0, projectAmount1);
                buttonStatus = "START";
            }
            if (fetchedDataB.length == projectAmount1) {
                localStorage.counter = fetchedDataB.length;
                localStorage.totaltargets = projectAmount1;
                code = localStorage.getItem('LicenseCode');
                $.ajax({
                    type: 'POST',
                    url: "https://www.leadjetpack.com/vfli/li/api.php",
                    dataType: 'json',
                    data: { fetchedDataB: fetchedDataB, code: code },

                    success: function(data, status, xhr) {
                        alert("response was " + data);
                        chrome.windows.remove(tab1);
                    },
                    error: function(xhr, status, errorMessage) {
                        chrome.windows.remove(tab1);
                        $("#debug").append("RESPONSE: " + xhr.responseText + ", error: " + errorMessage);
                    }
                });
                chrome.windows.remove(tab1);
            }
            console.log(fetchedDataB);
        }
        if (response.cmd == "close") {
            alert("Stop collecting");
            chrome.windows.remove(tab1);
            return;
        }
        if (response.cmd == "amReady") {
            switch (status) {

                case "getLinks":
                    sendResponse({ cmd: "getLinks", amount: projectAmount1, data: fetchedDataB });
                    break;
                case "pause":
                    console.log("stoped");
                    break

            }
        }

    }
);


/* processing */
var follow1, like1, invite1, totalProcess;
var next = 0;
var inviteC = 0;
var followC = 0;
var likeC = 0;
var fechedDataA = [];
var collecteddata = [];
var fechedData = [];
var funcProcess = function(follow, like, invite, getresult, pro) {
    follow1 = follow;
    like1 = like;
    invite1 = invite;
    getresult1 = getresult;
    totalProcess = pro;
    console.log(getresult1);
    console.log(like1, follow1, invite1);
    window.localStorage.inviteC = 0;
    window.localStorage.followC = 0;
    window.localStorage.visitC = 0;
    window.localStorage.likeC = 0;
    var config = {

        url: 'https://www.linkedin.com' + getresult1[0].url,
        width: 1200,
        height: 700,
        top: 10,
        left: 10,
        type: "popup"
    }

    chrome.windows.create(config, function(wnd) {
        position = "start";
        chrome.tabs.onUpdated.addListener(
            function(tabId, changeInfo, tab) {
                tab2 = wnd.tabs[0].id;
                if (changeInfo.status === "complete" && wnd.tabs[0].id == tabId) {
                    chrome.tabs.executeScript(tabId, { file: "process.js", runAt: "document_idle" },
                        function(result) {
                            // Process |result| here (or maybe do nothing at all).
                        }
                    );
                }
            }
        );

    });


};

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.msg == "startProc") funcProcess(request.follow, request.like, request.invite, request.getresult, request.pro);
        if (request.process == "start") {
            status = oldStatus;
            position = oldPosition;
            chrome.tabs.sendMessage(tab2, { cmd: "start" }, function(response) {});
            console.log("start");

        }
        if (request.process == "stop") {
            oldStatus = status;
            oldPosition = position;;
            position = "pause"
            status = "pause";
            chrome.tabs.sendMessage(tab2, { cmd: "stop" }, function(response) {});
            console.log("stop");
        }
    }
);
var fetchdataB = false;

chrome.runtime.onMessage.addListener(
    function(response, sender, sendResponse) {
        if (response.command && position == "pasue") retun;
        if (response.command == "dataReady") {

            fetchdataB = response.fechedDataB[0];
            fechedData = fechedData.concat(response.fechedDataB);
            console.log(fechedData);
            var dt = new Date();
            fetchdataB.visted = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
        }
        if (totalProcess == next) {
            var uniqueArray = removeDuplicates(fechedData, "url");
            code = localStorage.getItem('LicenseCode');

            function removeDuplicates(originalArray, prop) {
                var newArray = [];
                var lookupObject = {};

                for (var i in originalArray) {
                    lookupObject[originalArray[i][prop]] = originalArray[i];
                }

                for (i in lookupObject) {
                    newArray.push(lookupObject[i]);
                }
                return newArray;
            }



            $.ajax({
                type: 'POST',
                url: "https://www.leadjetpack.com/vfli/li/profileApi.php",
                dataType: 'json',
                data: { uniqueArray: uniqueArray, code: code },

                success: function(data, status, xhr) {
                    alert("response was " + data);

                },
                error: function(xhr, status, errorMessage) {

                    $("#debug").append("RESPONSE: " + xhr.responseText + ", error: " + errorMessage);
                }
            });

            console.log(uniqueArray);
            chrome.windows.remove(tab2);
        }
        if (response.command == "inviteCounter") {
            inviteC += 0.5;
            console.log("invite=" + inviteC);
            localStorage.inviteC = inviteC;

            var dt = new Date();
            if (fetchdataB) fechedData[next].dinvite = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();

        }
        if (response.command == "followCounter") {
            followC += 1;
            console.log("follow=" + followC);
            localStorage.followC = followC;
            var dt = new Date();
            if (fetchdataB) fechedData[next].dfollow = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
        }
        if (response.command == "likeCounter") {
            likeC += 1;
            console.log("follow=" + likeC);
            localStorage.likeC = likeC;
            var dt = new Date();
            if (fetchdataB) fechedData[next].dlike = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();

            /* var dt = new Date();
            fechedData[next].dlike = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
 */
        }

        if (response.commmand == "doLike") {
            position = "likeDone";
            //position = "start";
            var profileurl = response.profileurl;
            sendResponse({ commmand: "onProfileDoLike", profileurl: profileurl, follow: follow1, like: like1, invite: invite1, comment: comment });
        } else
        if (response.commmand == "returnbackUrl") {
            position = "doingFollowing";
            var profileurl = response.profileurl;
            sendResponse({ commmand: "returnedUrl", profileurl: profileurl });
        } else
        if (response.commmand == "nextProfile") {

            position = "start";
            next += 1;
            localStorage.nextC = next;

            urll = 'https://www.linkedin.com' + getresult1[next].url;
            sendResponse({ commmand: "changeUrl", url: urll });
        } else if (response.commmand == "iAmReady") {
            switch (position) {
                case "start":
                    var comment = localStorage.getItem('comment');
                    sendResponse({ commmand: "actionPerform", follow: follow1, like: like1, invite: invite1, comment: comment });
                    break;

                case "doingFollowing":
                    sendResponse({ commmand: "doFollow", follow: follow1, like: like1, invite: invite1, comment: comment });
                    break;
                case "likeDone":
                    sendResponse({ commmand: "likedone", follow: follow1, like: like1, invite: invite1, comment: comment });
                    break;
            }
        }

    }
);
//location.href = 'https://www.linkedin.com' + getresult1[next].url;
//