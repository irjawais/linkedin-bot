-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 10, 2017 at 05:26 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newextention`
--

-- --------------------------------------------------------

--
-- Table structure for table `TARGETS`
--

CREATE TABLE `TARGETS` (
  `code` varchar(1000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `fullname` varchar(1000) DEFAULT NULL,
  `firstname` varchar(1000) DEFAULT NULL,
  `lastname` varchar(1000) DEFAULT NULL,
  `headline` varchar(1000) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `company` varchar(1000) DEFAULT NULL,
  `position` varchar(1000) DEFAULT NULL,
  `datevisit` varchar(1000) DEFAULT NULL,
  `datefollow` varchar(1000) DEFAULT NULL,
  `datelike` varchar(1000) DEFAULT NULL,
  `dateinvite` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TARGETS`
--

INSERT INTO `TARGETS` (`code`, `url`, `fullname`, `firstname`, `lastname`, `headline`, `location`, `company`, `position`, `datevisit`, `datefollow`, `datelike`, `dateinvite`) VALUES
('', 'https://www.linkedin.com/in/aysar-jamama-6519b074/', 'Aysar jamama', 'jamama', NULL, 'Nursing lecturer at alghad college', 'Saudi Arabia', '', 'lecturerNursing lecturernursing', 'NULL', 'NULL', 'NULL', ''),
('', 'https://www.linkedin.com/in/abdalla-malin-34799bb0/', 'abdalla malin', 'malin', NULL, '--', 'Somalia', '', 'teacher jamama intrmediate ', '2017/10/9', 'NULL', 'NULL', ''),
('T123', 'https://www.linkedin.com/in/aysar-jamama-6519b074/', 'Aysar jamama', 'jamama', NULL, 'Nursing lecturer at alghad college', 'Saudi Arabia', '', 'lecturerNursing lecturernursing', 'NULL', 'NULL', 'NULL', ''),
('T123', 'https://www.linkedin.com/in/abdalla-malin-34799bb0/', 'abdalla malin', 'malin', NULL, '--', 'Somalia', '', 'teacher jamama intrmediate ', '2017/10/9', 'NULL', 'NULL', ''),
('T123', 'https://www.linkedin.com/in/aysar-jamama-6519b074/', 'Aysar jamama', 'jamama', NULL, 'Nursing lecturer at alghad college', 'Saudi Arabia', '', 'lecturerNursing lecturernursing', 'NULL', 'NULL', 'NULL', ''),
('T123', 'https://www.linkedin.com/in/abdalla-malin-34799bb0/', 'abdalla malin', 'malin', NULL, '--', 'Somalia', '', 'teacher jamama intrmediate ', '2017/10/9', 'NULL', 'NULL', '');

-- --------------------------------------------------------

--
-- Table structure for table `TARGETS_ARCHIVE`
--

CREATE TABLE `TARGETS_ARCHIVE` (
  `lis` varchar(100) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `head` varchar(1000) DEFAULT NULL,
  `loc` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TARGETS_ARCHIVE`
--

INSERT INTO `TARGETS_ARCHIVE` (`lis`, `url`, `name`, `head`, `loc`) VALUES
('T123', '/in/aysar-jamama-6519b074/', 'Aysar jamama', 'Nursing lecturer at alghad college', 'Saudi Arabia'),
('T123', '/in/abdalla-malin-34799bb0/', 'abdalla malin', '--', 'Somalia'),
('T123', '/in/agbozo-jamama-jamama-b9074661/', 'Agbozo Jamama Jamama', '--', 'Ghana'),
('T123', '/in/abdelfatah-jamama-94b5003/', 'Abdelfatah Jamama', 'IT Architect', 'Paris Area, France'),
('T123', '/in/naser-bamarhool-15938119/', 'Naser Bamarhool', '--', 'Saudi Arabia'),
('T123', '/in/jamama-kollie-4a041449/', 'Jamama Kollie', 'Student at ABC University', 'Liberia'),
('T123', '/in/jamama-ben-b6965a32/', 'jamama ben', 'Independent Civic & Social Organization Professional', 'Liberia'),
('T123', '/in/jamama-ffifiteen-37050213a/', 'jamama ffifiteen', 'idk at idk llc', 'Greater New York City Area'),
('T123', '/in/aysar-jamama-6519b074/', 'Aysar jamama', 'Nursing lecturer at alghad college', 'Saudi Arabia'),
('T123', '/in/abdalla-malin-34799bb0/', 'abdalla malin', '--', 'Somalia'),
('T123', '/in/agbozo-jamama-jamama-b9074661/', 'Agbozo Jamama Jamama', '--', 'Ghana'),
('T123', '/in/abdelfatah-jamama-94b5003/', 'Abdelfatah Jamama', 'IT Architect', 'Paris Area, France'),
('T123', '/in/naser-bamarhool-15938119/', 'Naser Bamarhool', '--', 'Saudi Arabia'),
('T123', '/in/jamama-kollie-4a041449/', 'Jamama Kollie', 'Student at ABC University', 'Liberia'),
('T123', '/in/jamama-ben-b6965a32/', 'jamama ben', 'Independent Civic & Social Organization Professional', 'Liberia'),
('T123', '/in/jamama-ffifiteen-37050213a/', 'jamama ffifiteen', 'idk at idk llc', 'Greater New York City Area'),
('T123', '/in/kati-jamama-9a034829/', 'kati jamama', 'Student at Albertus Magnus College', 'United States'),
('T123', '/in/sadgda-jamama-b2b512136/', 'Sadgda Jamama', '--', 'Germany'),
('T123', '/in/manuel-jamama-1a602a120/', 'Manuel Jamama', 'Frequentou a Instituto superior maria mÃ£e de africa', 'Mozambique'),
('T123', '/in/nedal-jamama-665a78101/', 'nedal jamama', 'Graphic Designer at on print', 'Nazareth Area, Israel'),
('T123', '/in/khalid-bhat-03002656/', 'khalid Bhat', 'Recruitment Professional -', 'Pune Area, India'),
('T123', '/in/dr-khalid-hussain-bhat-76632446/', 'Dr. Khalid Hussain Bhat', 'Research Associate', 'Hyderabad Area, India'),
('T123', '/in/khalid-bhat-519789110/', 'Khalid Bhat', 'Online offline trainings--cisco. Microsoft. ITIL juniper Linux, software development.', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-5311854/', 'Khalid Bhat', 'Head Corporate Affairs Reliance Jio Info comm Ltd,Jammu', 'Jammu Area, India'),
('T123', '/in/khalid-bhat-b91858142/', 'khalid bhat', 'SSM College of engineering and technology', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-09040464/', 'Khalid Bhat', 'CEO of Dreamy Destinations Pvt limited Srinagar Jammu and Kashmir', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-66a4281a/', 'khalid bhat', 'Scientist at agriculture university of KASHMIR', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-62bab662/', 'Khalid Bhat', 'Student at National istituteof Technology Srinagar', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-b1a849ab/', 'khalid bhat', 'Working at muscat intl airport', 'Sultanate of Oman'),
('T123', '/in/khalid-bhat-a03b4013a/', 'Khalid Bhat', 'Managing Director at Discover Indian tourism', 'Srinagar Area, India'),
('T123', '/in/khalid-bhat-a97683148/', 'Khalid Bhat', 'Senior Manager at HFCL Infotel Ltd.', 'Jammu Area, India'),
('T123', '/in/khalid-bhat-a4710473/', 'Khalid Bhat', 'Animation Professional', 'New Delhi Area, India'),
('T123', '/in/mudassarhussain/', 'Mudassar Hussain', 'Divisional Head, Training & Development', 'Pakistan'),
('T123', '/in/mudassar-ayyub-9a966a150/', 'Mudassar Ayyub', 'order Booker', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-hussain-977014151/', 'Mudassar hussain', 'Student at COMSATS Institute of Information and Technology', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mian-mudassar-sawan-gas-field-a5701028/', 'Mian Mudassar - Sawan Gas Field', 'Field Health,Safety and Environment Coordinator at OMV', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-saeed-23257348/', 'Mudassar Saeed', 'PM at Zemtrax Services', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-faheem-a5951391/', 'Mudassar Faheem', 'Student at BZU, multan, pakistan', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-habib-065667132/', 'Mudassar Habib', 'Business Operations Coordinator at Ufone', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-haider-ab634689/', 'Mudassar haider', 'LeadSupervisor at Captain PQ chemical industries Pvt. Limited', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-qayyum-233aa812a/', 'Mudassar Qayyum', 'Associate Software Engineer at i2c Inc.', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-kamal-92a75897/', 'Mudassar kamal', 'iOS & Software developer at Explore Recruitment Solutions, London ,UK', 'Pakistan'),
('T123', '/in/mudassarhussain/', 'Mudassar Hussain', 'Divisional Head, Training & Development', 'Pakistan'),
('T123', '/in/mudassar-ayyub-9a966a150/', 'Mudassar Ayyub', 'order Booker', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-hussain-977014151/', 'Mudassar hussain', 'Student at COMSATS Institute of Information and Technology', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mian-mudassar-sawan-gas-field-a5701028/', 'Mian Mudassar - Sawan Gas Field', 'Field Health,Safety and Environment Coordinator at OMV', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-saeed-23257348/', 'Mudassar Saeed', 'PM at Zemtrax Services', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-faheem-a5951391/', 'Mudassar Faheem', 'Student at BZU, multan, pakistan', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-habib-065667132/', 'Mudassar Habib', 'Business Operations Coordinator at Ufone', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-haider-ab634689/', 'Mudassar haider', 'LeadSupervisor at Captain PQ chemical industries Pvt. Limited', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-qayyum-233aa812a/', 'Mudassar Qayyum', 'Associate Software Engineer at i2c Inc.', 'Southern Punjab Multan, Pakistan'),
('T123', '/in/mudassar-kamal-92a75897/', 'Mudassar kamal', 'iOS & Software developer at Explore Recruitment Solutions, London ,UK', 'Pakistan'),
('T123', '/in/mudassar-hussain-416451108/', 'Mudassar Hussain', 'Senior CP Supervisor at Inspectest (Pvt) Ltd', 'Pakistan'),
('T123', '/in/muhammad-mudassar-khan-868a4045/', 'Muhammad Mudassar Khan', 'GM Operations at Ã‰lan', 'Lahore, Pakistan'),
('T123', '/in/mudassarkhanee/', 'Mudassar Khan PMPÂ®,PMI', 'MSc Electrical, Sr. Electrical Engineer / Manager Projects at Associated Technologies Pvt Ltd', 'Lahore, Pakistan'),
('T123', '/in/mudassar-malik-a366b2102/', 'Mudassar Malik', 'Training Instructor at (ICBT)', 'Lahore, Pakistan'),
('T123', '/in/mudassar-lone-49649816/', 'Mudassar Lone', 'President', 'Lahore, Pakistan'),
('T123', '/in/mudassar-muneer-37b5263b/', 'Mudassar Muneer', 'Entrepreneur', 'Northern Punjab Rawalpindi, Pakistan'),
('T123', '/in/mudassar-hussain-108124b5/', 'Mudassar Hussain', 'Assistant Manager GWP & GDP at US apparel & textiles unite#5', 'Lahore, Pakistan'),
('T123', '/in/mudassar-ehsan-chatha-918910b5/', 'Mudassar Ehsan Chatha', 'IP-RAN Eingineer on Telenor IP-Agg (One Asia) Swap Project in Planning &Implementation department at ZTE Enterprise.', 'Pakistan'),
('T123', '/in/iqbalmudassar/', 'Mudassar Iqbal', 'Magento Developer at Demac Media', 'Pakistan'),
('T123', '/in/mudassar-raja-905b5557/', 'Mudassar Raja', 'Retired Army Office and CEO of \"MY Travels\" Travel & Tour Agency at \"My Travels\" Travels & Tours', 'Pakistan'),
('T123', '/in/mudassar-raana-87414b76/', 'MUDASSAR RAANA', 'AVP/BOM at JS Bank', 'Pakistan'),
('T123', '/in/mudassarsalman/', 'Mudassar Salman', 'Quality Assurance, Strategic Sourcing & Manufacturing Specialist Dynamic Sportswear Pvt. Ltd.', 'Pakistan'),
('T123', '/in/mudassar-ayub-67735aba/', 'MUDASSAR AYUB', 'Student at National University of Sciences and Technology (NUST)', 'Pakistan'),
('T123', '/in/mrfarii/', 'Muhammad Farooq', 'Co-Owner at Gandhara Heritage', 'NWFP Peshawar, Pakistan'),
('T123', '/in/fahadrazanaqvi/', 'Syed Fahad Raza', 'Co -Manager at Google Business Group -Peshawar | Process Innovation Researcher', 'Pakistan'),
('T123', '/in/shoziart/', 'Shehroz Rashid', 'Co Founder at BinaryOps', 'Pakistan'),
('T123', '/in/bilalaahmad/', 'Bilal Ahmad', 'Co-Founder at Peshawar 2.0', 'Pakistan'),
('T123', '/in/babarkhanakhunzada/', 'Babar Khan Akhunzada', 'Founder at SecurityWall.co', 'Lahore, Pakistan'),
('T123', '/in/zameer-bukhari-852172a7/', 'zameer bukhari', 'Biomedical Engineer at Medizaq Imaging Co', 'Pakistan'),
('T123', '/in/arshad-khan-0768969b/', 'Arshad Khan', 'Co-founder at RoboHealth', 'NWFP Peshawar, Pakistan'),
('T123', '/in/muhammad-nawaz-6aa793b9/', 'Muhammad Nawaz', 'CO at Civil Secretariat, kpk', 'Pakistan'),
('T123', '/in/today-synario-231334148/', 'today synario', 'Co-Founder at todaysynario', 'NWFP Peshawar, Pakistan'),
('T123', '/in/mrfarii/', 'Muhammad Farooq', 'Co-Owner at Gandhara Heritage', 'NWFP Peshawar, Pakistan'),
('T123', '/in/fahadrazanaqvi/', 'Syed Fahad Raza', 'Co -Manager at Google Business Group -Peshawar | Process Innovation Researcher', 'Pakistan'),
('T123', '/in/shoziart/', 'Shehroz Rashid', 'Co Founder at BinaryOps', 'Pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `license` varchar(50) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `profileurl` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `license`, `email`, `profileurl`) VALUES
(1, '111', 'fdsdf@gmail.com', 'abc'),
(2, '22', 'dfdsfds@gmailsdfds.com', 'dfd'),
(3, '1', 'abc@abc.comdfd', 'fd'),
(4, 'T123', 'FDF@gas.co', 'dfds');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `license` (`license`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
