$(document).ready(function() {
    var LicenseCode = localStorage.getItem('LicenseCode');

    if ("LicenseCode" in localStorage) {
        openTab("menubaropen");
        openTab("CustomizerPage");
        $('#nav li a').css("background-color", "white");
        $('#processbtn').css("background-color", "#f0ad4e");
    } else {
        openTab("LoginPage");
        closeTab();
    }
});
//progress bar

$(document).ready(function() {
    window.setInterval(function() {
        projectnm = localStorage.getItem('projectName');

        document.getElementById("projectName").innerHTML = "Project : " + projectnm;
        var total = parseInt(localStorage.getItem('totaltargets'));
        if (!total) {
            total = 0;
        }
        var collected1 = parseInt(localStorage.getItem('counter'));
        if (!collected1) {
            collected1 = 0;
        }
        var width1 = (collected1 / total) * 100;
        if (!width1) {
            width1 = 0;
        }
        document.getElementById("progress-bar").style.width = width1 + "%";

        document.getElementById("TotalConnects").innerHTML = "Total : " + collected1 + " / " + total + " ( " + Math.round(width1) + " % )";

        nextC = parseInt(localStorage.getItem('nextC'));
        width2 = (nextC / total) * 100;
        document.getElementById("p11").style.width = width2 + "%";
        document.getElementById("p12").innerHTML = "VISIT : " + nextC;

        followC = parseInt(localStorage.getItem('followC'));
        width3 = (followC / total) * 100;
        document.getElementById("p21").style.width = width3 + "%";
        document.getElementById("p22").innerHTML = "FOLLOW : " + followC;

        likeC = parseInt(localStorage.getItem('likeC'));
        width4 = (likeC / total) * 100;
        document.getElementById("p31").style.width = width4 + "%";
        document.getElementById("p32").innerHTML = "LIKE : " + likeC;

        inviteC = parseInt(localStorage.getItem('inviteC'));
        width5 = (inviteC / total) * 100;
        document.getElementById("p41").style.width = width5 + "%";
        document.getElementById("p42").innerHTML = "INVITE : " + inviteC;

        document.getElementById("visited-p").innerHTML = nextC;
        document.getElementById("followed-p").innerHTML = followC;
        document.getElementById("liked-p").innerHTML = likeC;
        document.getElementById("invited-p").innerHTML = inviteC;

    }, 1000);

    $('#vclear').on('click', function() {
        localStorage.nextC = 0;
    })
    $('#fclear').on('click', function() {
        localStorage.followC = 0;
    })
    $('#lclear').on('click', function() {
        localStorage.likeC = 0;
    })
    $('#iclear').on('click', function() {
        localStorage.inviteC = 0;
    })
    $('#finish').on('click', function() {
        localStorage.comment = "";
        localStorage.counter = 0;
        localStorage.projectName = "";
        localStorage.totaltargets = "";
        localStorage.nextC = 0;
        localStorage.followC = 0;
        localStorage.likeC = 0;
        localStorage.inviteC = 0;
    })


});


document.getElementById("processbtn").onclick = function() {

    openTab("menubaropen");
    openTab("CustomizerPage");
};
document.getElementById("collectbtn").onclick = function() {

    openTab("menubaropen");
    openTab("InvitationRemoval");
};
document.getElementById("targetbtn").onclick = function() {

    openTab("menubaropen");
    openTab("NotLogged");
};
document.getElementById("settingbtn").onclick = function() {

    openTab("menubaropen");
    openTab("ProgressPage");
};
document.getElementById("logout1").onclick = function() {
    localStorage.removeItem('LicenseCode');
    openTab("LoginPage");
    closeTab();
};
document.getElementById("logout2").onclick = function() {
    localStorage.removeItem('LicenseCode');
    openTab("LoginPage");
    closeTab();
};
document.getElementById("logout3").onclick = function() {
    localStorage.removeItem('LicenseCode');
    openTab("LoginPage");
    closeTab();
};
document.getElementById("logout4").onclick = function() {
    localStorage.removeItem('LicenseCode');
    openTab("LoginPage");
    closeTab();
};

function openTab(id) {
    ShowLoading()
    tabContent = document.getElementsByClassName("TabContent");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    activeContent = document.getElementById(id);
    activeContent.style.display = "block";
    document.getElementById("Message").style.display = "none";

}

function closeTab() {
    tabContent = document.getElementsByClassName("menubar");


    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }

}

function ShowLoading() {
    var Message = document.getElementById("Message");
    Message.innerHTML = "<img src=\"images/loading.gif\"/>";
    Message.setAttribute("class", "success-message");
    Message.style.display = "inline-block";
    setTimeout(function() {
        if (Message.getElementsByTagName("img").length > 0) Message.style.display = "none";
    }, 5 * 1000);
}

$(function() {


    $('#nav li a').click(function() {

        $('#nav li a').css("background-color", "white");
        $($(this)).css("background-color", "#f0ad4e");
    });
});

/* LOGIN PAGE */
$(document).ready(function() {
    $('#loginform').on('submit', function() {
        ShowLoading();
        var LicenseCode = $('#licenseNo').val();
        $.ajax({
            type: 'POST',
            url: "https://www.leadjetpack.com/vfli/li/response.php",
            dataType: "json",
            data: { LicenseCode: LicenseCode },
            success: function(data) {
                if (data.status == 'ok') {
                    openTab("menubaropen");
                    openTab("CustomizerPage");
                    localStorage.LicenseCode = LicenseCode;
                    $('#nav li a').css("background-color", "white");
                    $('#processbtn').css("background-color", "#f0ad4e");
                } else {
                    $('#Message').text('Please Enter the correct Licence Ccde')
                }
            }
        });
        return false;
    });
});
var otherWindows = chrome.extension.getBackgroundPage();
$(document).ready(function() {
    var counter2 = localStorage.getItem('counter')
    $('#processinput').val(counter2);
    var projectN2 = localStorage.getItem('projectName')
    $('#projectN').val(projectN2);

    /* var otherWindows = chrome.extension.getBackgroundPage();
    buttonStatus = otherWindows.buttonStatus;
    $('#collectTarget').text(buttonStatus); */

    $('#projectform').on('submit', function() {



        if (otherWindows.buttonStatus == "STOP") {
            if (otherWindows.signal == "START") {
                chrome.extension.sendMessage({ msg: "start" });
                buttonStatus = otherWindows.signal;
                $('#collectTarget').text(buttonStatus);
                otherWindows.signal = "STOP"

            } else {
                otherWindows = chrome.extension.getBackgroundPage();
                chrome.extension.sendMessage({ msg: "stop" });
                buttonStatus = otherWindows.signal;
                $('#collectTarget').text(buttonStatus);

                otherWindows.signal = "START";
            }
            //chrome.extension.sendMessage({ msg: "stop" });
            // otherWindows.buttonStatus = "START";
            // buttonStatus = otherWindows.buttonStatus;
            $('#collectTarget').text(buttonStatus);
            return false;
        } else {

            otherWindows.buttonStatus = "STOP"
            otherWindows.signal = "STOP"
            var projectName = $('#projectName2').val();
            window.localStorage.projectName = projectName;
            var projectUrl = $('#projectUrl').val();
            var projectAmount = $('#projectAmount').val();
            // $('#collectTarget').text(buttonStatus);  
            chrome.extension.sendMessage({ msg: "startFunc", projectName: projectName, projectUrl: projectUrl, projectAmount: projectAmount });
            //window.open(projectUrl,'popUpWindow','height=700,width=1200,left=10,top=10,,scrollbars=yes,menubar=no');
            return false;
        }
    });
    $('#defaultMessage').on('click', function() {
        var comment = $('#comment').val();
        localStorage.comment = comment;
        $('#defaultMessage').text("SAVED");
        return false;
    })
});
var otherWindows = chrome.extension.getBackgroundPage();
$(document).ready(function() {
    var getresult = [];
    var pro;
    $('#collectTarget').text(otherWindows.signal);
    $('#startProcess').on('click', function() {
        if (otherWindows.processStatus == "STOP") {
            if (otherWindows.processSignal == "START") {
                chrome.extension.sendMessage({ process: "start" });
                processStatus = otherWindows.processSignal;
                $('#startProcess').text(processStatus);
                otherWindows.processSignal = "STOP"

            } else {
                otherWindows = chrome.extension.getBackgroundPage();
                chrome.extension.sendMessage({ process: "stop" });
                processStatus = otherWindows.processSignal;
                $('#startProcess').text(processStatus);

                otherWindows.processSignal = "START";
            }
            //chrome.extension.sendMessage({ msg: "stop" });
            // otherWindows.buttonStatus = "START";
            // buttonStatus = otherWindows.buttonStatus;
            $('#collectTarget').text(processStatus);
            return false;
        } else {

            otherWindows.processStatus = "STOP"
            otherWindows.processSignal = "STOP"

            if ($('#follow').is(":checked")) {
                var follow = $('#follow').val();
            }
            if ($('#like').is(":checked")) {
                var like = $('#like').val();
            }
            if ($('#invite').is(":checked")) {
                var invite = $('#invite').val();
            }
            pro = $('#processinput').val();
            var code1 = localStorage.getItem('LicenseCode');

            function getPosts() {

                $.ajax({
                    url: 'https://www.leadjetpack.com/vfli/li/DataApi.php',
                    data: { action: 'getGroups', pro: pro, code1: code1 },
                    type: 'post',
                    dataType: 'json',
                    success: function(output) {
                        getresult = output;
                        console.log(getresult);
                        chrome.extension.sendMessage({ msg: "startProc", follow: follow, like: like, invite: invite, getresult: getresult, pro: pro });
                    }
                });
            }
        }
        getPosts();
        return false;
    })
    $('#export').on('click', function() {
        var proN = localStorage.getItem('projectName');
        code1 = localStorage.getItem('LicenseCode');
        window.open("https://www.leadjetpack.com/vfli/li/csvexport.php?proN=" + proN + "&code1=" + code1);

    })
});

/* $(document).ready(function() {
    $('#file-upload').on("click", function(e) {
        e.preventDefault(); //form will not submitted  
        $.ajax({
            url: "http://localhost/li/csvImport.php",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                if (data == 'Error1') {
                    alert("Invalid File");
                } else if (data == "Error2") {
                    alert("Please Select File");
                } else {
                    $('#employee_table').html(data);
                }
            }
        })
    });
}); */